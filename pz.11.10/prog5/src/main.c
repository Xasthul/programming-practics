int main(){
	int year = 2100;
	int result; // 0 - обычный, 1 - высокостный
	if(year % 4 != 0){
		result = 0;
	} else if(year % 100 != 0){
		result = 1;
	} else if(year % 400 == 0){
		result = 1;
	} else {
		result = 0;
	}

	// int result = (year % 4 != 0) ? 0 : (year % 100 != 0) ? 1 : (year % 400 == 0) ? 1 : 0;

	return 0;
}
