int main(){
	int k = 1;
	double y = 0;
	double eps = 0.00000001;

	float div;
	int i = 1;
	int temp = 1;
	do{
		div = k * 1.0f / i;
		y += temp ? div : -div;
		i *= 2;
		temp = !temp;
	}while(div > eps);

	return 0;
}
