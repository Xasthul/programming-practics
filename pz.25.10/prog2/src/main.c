int main(){
	int k = 2;
	int n = 5;
	int y = 0;

	for (int i = 1; i <= n; i++){
		int temp = 1;
		for (int j = 0; j < i; j++){
			temp *= k;
		}
		y += temp;
	}

	return 0;
}
