void sum_matrix(int m1[], int m2[], int m3[], int size);

int get_by_ij(int i, int j, int cols);

int main(){

#define COLS 3
#define ROWS 3
#define SIZE COLS*ROWS

	int m1[COLS][ROWS] = { {1,2,3}, {4,5,6}, {7,8,9} };
	int m2[COLS][ROWS] = { {9,8,7}, {6,5,4}, {3,2,1} };
	int m3[COLS][ROWS] = {};

	int m1_raw[SIZE] = {};
	int m2_raw[SIZE] = {};
	int m3_raw[SIZE] = {};

	for(int i = 0; i < COLS; i++){
		for(int j = 0; j < ROWS; j++){
			m1_raw[get_by_ij(i, j, COLS)] = m1[i][j];
			m2_raw[get_by_ij(i, j, COLS)] = m2[i][j];
		}
	}
	sum_matrix(m1_raw, m2_raw, m3_raw, SIZE);

	for(int i = 0; i < COLS; i++){
		for(int j = 0; j < ROWS; j++){
			m3[i][j] = m3_raw[get_by_ij(i, j, COLS)];
		}
	}

	return 0;
}

int get_by_ij(int i, int j, int cols){

	return i * cols + j;
}

void sum_matrix(int m1[], int m2[], int m3[], int size){
	for(int i = 0; i < size; i++){
		m3[i] = m1[i] + m2[i];
	}
}

