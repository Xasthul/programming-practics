#include <stdlib.h>
#include <time.h>

void sort_array(int array[], size_t size);

void random_array(int array[], size_t size);

int main() {
	srand(time(NULL));

	#define SIZE 5
	int arr[SIZE];
	random_array(arr, SIZE);
	sort_array(arr, SIZE);

	return 0;
}

void random_array(int array[], size_t size){
	for(int i = 0; i < size; i++){
		array[i] = rand() % 201 - 100;
	}
}

void sort_array(int array[], size_t size){
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size - 1; j++){
			if(array[j] > array[j+1]){
				int temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
			}
		}
	}


}
