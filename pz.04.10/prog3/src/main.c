int main(){
	unsigned int h1 = 12; // 12:04:07
	unsigned int m1 = 4;
	unsigned int s1 = 7;
	unsigned int sec1 = s1 + m1*60 + h1*3600; // 43447

	unsigned int h2 = 14; // 14:03:59
	unsigned int m2 = 3;
	unsigned int s2 = 59;
	unsigned int sec2 = s2 + m2*60 + h2*3600; // 50639

	unsigned int h3 = (sec2 - sec1) / 3600; // 01:59:52
	unsigned int m3 = ((sec2 - sec1) - h3 * 3600) / 60;
	unsigned int s3 = (sec2 - sec1) - h3 * 3600 - m3 * 60;

	return 0;
}
